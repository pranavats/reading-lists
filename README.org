Reading lists on various subjects.

* Philosophy
  + [[./philosophy/logic/list.org][Logic]]
  + [[./philosophy/mathematics/list.org][Mathematics]]
  + [[./philosophy/science/list.org][Science]]
* [[./history/list.org][History]]
* [[./mathematics/list.org][Mathematics]]
* [[./science/list.org][Science]]
